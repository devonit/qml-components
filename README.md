# README

## Building and Installing

    qmake
    sudo make install

## Usage

To use the installed components with your QML project, add the following
import line to your QML file:

    import DevonIT.Controls 1.0
    import DevonIT.Validators 1.0

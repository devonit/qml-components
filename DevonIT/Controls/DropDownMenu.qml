import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

Item {
    id: dropDown

    width: 200
    height: 22
    implicitHeight: height

    z: 1

    // acceptableInput:
    //      Read-only property that contains the current validation state for
    //      the selection box.  Mainly used with editable drop downs.
    //
    property alias acceptableInput: selectionBox.acceptableInput

    // currentText:
    //      The currently selected item.
    //
    property alias currentText: selectionBox.text

    // defaultSelection:
    //      The entry that will appear in the selection box on load.
    //
    property string defaultSelection

    // editable:
    //      The selection box doubles as a text entry field for adding items.
    //
    property bool editable: false


    // editText:
    //      The current text in the selection box if it's editable.
    //
    property alias editText: selectionBox.text

    // inputMask:
    //      Restrict input to a certain input pattern. Kind of like validators, but retarded.
    //
    property alias inputMask: selectionBox.inputMask

    // maximumLength:
    //      Restrict input length in the selection box to... this number.
    //
    property alias maximumLength: selectionBox.maximumLength

    // menuHeight:
    //      Controls how large the drop down menu is, if you're into that kind
    //      of thing.
    //
    property int menuHeight: 200

    // model:
    //      The data model to be used in the menu. Currently, only works with
    //      QStringList.
    //
    property alias model: dropDownModel.model

    // validator:
    //      Set a validator for the text in the selection box.
    //
    property alias validator: selectionBox.validator


    // accepted:
    //      Raised when text input in the selection box passed the
    //      validator/enter was pressed.
    //
    signal accepted


    // XXX Toggle the drop down menu on spacebar and return key pressed, or
    //     close it with escape. The future is now!
    Keys.onSpacePressed: {
        dropDownList.toggleMenu()
    }

    Keys.onReturnPressed: {
        dropDownList.toggleMenu()
    }

    Keys.onEscapePressed: {
        dropDownList.closeMenu()
    }

    // find
    //      item: string
    //      return integer
    //
    // Returns index of given item, or -1 if not found.
    //
    function find(item) {
        var length = dropDownModel.count - 1;
        for (var i = 0; i < length; ++i) {
            if (item === dropDownModel.get(i).name) {
                return i;
            }
        }

        return -1;
    }

    ListModel {
        id: dropDownModel

        property variant model

        // XXX Why not just 'onModelChanged' you might ask? Well,
        //     defaultSelection is null in that handler when the component
        //     is first initialized. Why? I don't know. Probably because the
        //     ice cream has no bones.
        Component.onCompleted: updateModel();
        onModelChanged: updateModel();

        function updateModel() {
            dropDownModel.clear();
            for (var i = 0; i < model.length; i++) {
                if (model[i] === dropDown.defaultSelection) {
                    dropDownView.currentIndex = i;
                    selectionBox.text = model[i];
                }
                dropDownModel.append({ "name" : model[i] })
            }
        }
    }

    // dropDownView Delegate
    Component {
        id: dropDownDelegate
        Rectangle {
            id: dropDownItem
            width: dropDown.width
            height: dropDown.height
            color: "transparent"
            Column {
                anchors {
                    verticalCenter: parent.verticalCenter
                    left: parent.left
                    leftMargin: 5
                }

                Text { 
                    text: name
                    font.pixelSize: 12
                }
            }

            MouseArea {
                anchors.fill: parent
                hoverEnabled: true

                onEntered: {
                    dropDownItem.color = "#ccc"
                }

                onExited: {
                    dropDownItem.color = "transparent"
                }

                onClicked: {
                    dropDownView.currentIndex = index
                    dropDown.currentText = name
                    dropDownList.visible = false
                    selectionBox.text = name
                }
            }
        }
    }

    // What did you choose from the drop down menu? It will show up in this box!
    // Also, if (editable = true) you can type in your own abominations. 
    TextField {
        id: selectionBox
        readOnly: !dropDown.editable
        height: 22
        verticalAlignment: TextInput.AlignVCenter
        activeFocusOnTab: !readOnly
        font.pixelSize: 12

        anchors {
            left: dropDown.left
            right: dropDownButton.left
        }

        style: TextFieldStyle {
            textColor: "black"
            background: Rectangle {
                color: "#fff"
                border.color: "#333"
                border.width: 1
            }
        }

        onAccepted: {
            dropDown.accepted()
        }
    }

    // Drop down trigger.
    Button {
        id: dropDownButton
        width: 22
        height: selectionBox.height
        z: dropDown.z
        tooltip: qsTr("Expand the drop down menu.")
        iconName: "go-down"
        checkable: true

        anchors {
            top: selectionBox.top
            right: dropDown.right
        }

        onClicked: {
            dropDownList.toggleMenu()
        }

        style: ButtonStyle {
            background: Rectangle {
                implicitWidth: 22
                implicitHeight: selectionBox.height

                color: control.checked ? "#444" : "#888"
                radius: 4

                Rectangle {
                    implicitWidth: 10
                    implicitHeight: selectionBox.height
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    color: parent.color
                }
            }

            label: Image {
                source: "image://theme/" + control.iconName
            }
        }
    }

    // Drop down menu.
    Rectangle {
        id: dropDownList
        width: dropDown.width
        height: dropDown.menuHeight
        anchors.top: selectionBox.bottom
        anchors.left: selectionBox.left
        clip: true
        visible: false
        border.width: 1
        border.color: "#333"

        color: "#fff"
        z: dropDown.z

        ScrollView {
            anchors.fill: parent
            z: dropDown.z

            ListView {
                id: dropDownView

                anchors.fill: parent
                anchors.leftMargin: 2
                anchors.rightMargin: 5

                model: dropDownModel
                delegate: dropDownDelegate
                focus: true
                z: dropDown.z
                highlight: Rectangle { 
                        color: "#0088CC"
                        width: dropDownList.width
                        height: dropDown.height
                }
                
                highlightFollowsCurrentItem: true
                activeFocusOnTab: true

                Keys.onUpPressed: {
                    if (dropDownView.currentIndex > 0) {
                        --dropDownView.currentIndex;
                    } else {
                        dropDownView.currentIndex = 0;
                    }
                }

                Keys.onDownPressed: {
                    if (dropDownView.currentIndex < dropDownView.count) {
                        ++dropDownView.currentIndex;
                    } else {
                        dropDownView.currentIndex = (dropDownView.count - 1)
                    }
                }

                Keys.onReturnPressed: {
                    selectionBox.text = dropDownModel.get(dropDownView.currentIndex).name;
                    dropDownList.closeMenu();
                }

                Keys.onEscapePressed: {
                    dropDownList.closeMenu();
                }
            }
        }

        function toggleMenu() {
            if (dropDownList.visible) {
                closeMenu();
            } else {
                openMenu();
            }
        }

        function closeMenu() {
            dropDownList.visible = false
            dropDownView.focus = false
            selectionBox.forceActiveFocus(Qt.PopupFocusReason)
        }

        function openMenu() {
            dropDownList.visible = true
            selectionBox.focus = false
            dropDownView.forceActiveFocus(Qt.PopupFocusReason)
        }
    }
}

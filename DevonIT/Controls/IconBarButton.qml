import QtQuick 2.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
import QtQuick.Layouts 1.1

Button {
    property string checkedColor: "#0088CC"
    property string uncheckedColor: "transparent"
    property string textColor: "#ffffff"
    property int textSize: 10
    property int iconWidth: 32
    property int iconHeight: 32
    property int radius: 0

    width: 92
    height: 80

    style: ButtonStyle {
        background: Rectangle {
            implicitWidth: width
            implicitHeight: height
            border.width: control.activeFocus ? 1 : 0
            border.color: control.activeFocus ? checkedColor : uncheckedColor
            color: (control.checked || control.hovered) ? checkedColor : uncheckedColor
            radius: control.radius
        }
        label: ColumnLayout {
                spacing: 2

                anchors {
                    centerIn: parent
                    fill: parent
                }

                Image {
                    Layout.alignment: Qt.AlignHCenter

                    sourceSize.width: iconWidth
                    sourceSize.height: iconHeight

                    verticalAlignment: Image.AlignBottom
                    horizontalAlignment: Image.AlignHCenter

                    fillMode: Image.PreserveAspectFit
                    source: "image://theme/" + control.iconName
                }
                Text {
                    Layout.alignment: Qt.AlignHCenter

                    text: control.text
                    color: textColor
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: textSize
                }
            }
    }
}


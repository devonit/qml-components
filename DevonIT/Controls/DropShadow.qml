import QtQuick 2.2

// Drop shadow
Rectangle {
    z: -1
    width: parent.width
    height: parent.height
    radius: 2
    anchors {
        top: parent.top
        left: parent.left
        topMargin: 5
        leftMargin: 4
    }
    gradient: Gradient {
        GradientStop {
            position: 0.00;
            color: "#22000000";
        }
        GradientStop {
            position: 1.00;
            color: "#1A000000";
        }
    }
}

import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.1

Rectangle {
    readonly property string invalidPropertyColor: "#A60800"

    width: 10
    implicitWidth: width
    height: 10
    implicitHeight: height
    radius: 10
    z: 0

    color: invalidPropertyColor
    visible: false
}

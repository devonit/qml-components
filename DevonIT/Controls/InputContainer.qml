import QtQuick 2.2
import QtQuick.Layouts 1.1

Item {
    id: container

    Layout.fillWidth: true

    anchors {
        left: parent.left
        right: parent.right
    }
    height: 22

    property int labelWidth: 120
    property string label
    default property alias widgets: widgetRow.children
    property bool enableValidator: indicator.enabled
    property alias validationExpression: indicator.expression
    property bool mirrorLabel: false

    RowLayout {
        id: innerRow
        z: 0

        anchors.fill: parent

        spacing: 3

        FieldLabel {
            id: rowLabel
            width: labelWidth
            text: container.label
            z: 0

            Layout.minimumWidth: labelWidth
            Layout.maximumWidth: labelWidth
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter

            Binding on visible {
                value: !container.mirrorLabel
            }
            Binding on enabled {
                value: !container.mirrorLabel
            }
        }

        RowLayout {
            id: widgetRow
            spacing: 3
            width: 400
            anchors {
                top: innerRow.top
                bottom: innerRow.bottom
            }

            Layout.minimumWidth: width
            Layout.maximumWidth: width
            Layout.alignment: Qt.AlignLeft | Qt.AlignVCenter
            Layout.fillWidth: true

            z: 0
        }

        // Mirrored label for use with radio buttons or checkboxes.
        FieldLabel {
            id: rowLabelMirror
            width: labelWidth
            text: container.label
            z: 0

            Binding on visible {
                value: container.mirrorLabel
            }
            Binding on enabled {
                value: container.mirrorLabel
            }
        }
    }

    InvalidInputIndicator {
        id: indicator
        z: 0
        anchors.right: container.right
        anchors.verticalCenter: innerRow.verticalCenter

        property bool expression: true

        // Take the negation of the expression so that we can write the validation
        // expression more naturally, ex: foo.length > 0, instead of !(foo.length > 0)
        Binding on visible {
            value: !indicator.expression
        }
    }
}

import QtQuick 2.1

Rectangle {
    color: "#333"
    width: parent.width
    height: 11
    radius: 3.0

    property string title
    readonly property int sectionHeight: height + flatEdge.height

    anchors {
        top: parent.top
        left: parent.left
    }

    Rectangle {
        id: flatEdge
        color: "#333"
        width: parent.width
        height: 16
        anchors {
            top: parent.verticalCenter
        }
    }

    Text {
        anchors {
            top: parent.top
            left: parent.left
            verticalCenter: parent.verticalCenter
            leftMargin: 4
            bottomMargin: 4
            topMargin: 2
        }
        text: parent.title
        font.weight: Font.Bold
        font.pixelSize: 14
        color: "#fafafa"
    }
}

import QtQuick 2.2
import QtQuick.Layouts 1.1
import DevonIT.Controls 1.0

Rectangle {
    width: 600
    height: 100

    // widgets:
    //      Takes a list of components, which will then appear in the section.
    //      By default, you can just do:
    //
    //      Section {
    //          Thing { }
    //            .
    //            .
    //            .
    //      }
    //
    default property alias widgets: grid.data


    // title:
    //      The text that will appear in the section header.
    //
    property alias title: header.title


    // columns:
    //      Internally, section body has a grid layout. This controls the number of
    //      columns you want. The default is 2.
    //
    property alias columns: grid.columns


    // columnSpacing:
    //      The amount of space you want between the columns in the section
    //      body. The default is 10.
    //
    property alias columnSpacing: grid.columnSpacing


    // rowSpacing:
    //      The amount of space you want between rows in the section body. The
    //      default is 4.
    //
    property alias rowSpacing: grid.rowSpacing

    border.color: "#d0d0d0"
    border.width: 1
    color: "#eee"
    radius: 3.0

    SectionHeader {
        id: header
    }

    GridLayout {
        id: grid

        columns: 2
        columnSpacing: 10
        rowSpacing: 4

        anchors {
            topMargin: header.sectionHeight
            bottomMargin: 4
            rightMargin: 10
            leftMargin: 10
            fill: parent
        }
    }

    DropShadow {}
}

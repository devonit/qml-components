import QtQuick 2.1
import QtQuick.Layouts 1.1

Rectangle {
    height: 1
    width: parent.width
    color: "#d0d0d0"
    Layout.columnSpan: 2
    Layout.preferredWidth: parent.width
}

import QtQuick 2.1
import QtQuick.Controls 1.1

RegExpValidator {
    regExp: /^([1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-2][0-9]{2}|653[0-4][0-9]|6535[0-5])$/
}

CONFIG += nomake
TARGET -= devonit-qml-components

CONTROLS += \
    DevonIT/Controls/DropDownMenu.qml \
    DevonIT/Controls/DropShadow.qml \
    DevonIT/Controls/FieldLabel.qml \
    DevonIT/Controls/IconBar.qml \
    DevonIT/Controls/IconBarButton.qml \
    DevonIT/Controls/InputContainer.qml \
    DevonIT/Controls/InvalidInputIndicator.qml \
    DevonIT/Controls/Rule.qml \
    DevonIT/Controls/Section.qml \
    DevonIT/Controls/SectionHeader.qml \
    DevonIT/Controls/qmldir

VALIDATORS += \
    DevonIT/Validators/HostnameValidator.qml \
    DevonIT/Validators/IPv4ORHostnameValidator.qml \
    DevonIT/Validators/IPv4Validator.qml \
    DevonIT/Validators/PortValidator.qml \
    DevonIT/Validators/qmldir

isEmpty(QML_IMPORT_ROOT) {
    equals(QT_MAJOR_VERSION, 4): QML_IMPORT_ROOT = $$[QT_INSTALL_IMPORTS]
    equals(QT_MAJOR_VERSION, 5): QML_IMPORT_ROOT = $$[QT_INSTALL_QML]
}

target.files = $${CONTROLS}
target.path = $${QML_IMPORT_ROOT}/DevonIT/Controls

validators.files = $${VALIDATORS}
validators.path = $${QML_IMPORT_ROOT}/DevonIT/Validators

INSTALLS += target validators
